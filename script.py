
# S - Сумма займа
# M - Количество месяцев
# X - Процент

def annu(S, M, X):
    result = []
    M = int(M)
    p = X/M/100
    AP = S * (p + p / (pow(1+p, M) - 1))
    for i in range(M):
        result.append ( ( round(S, 2), round(S * p, 2), round(AP - S * p, 2), round(AP, 2), ) )
        S = S - AP + S * p
    return result

if __name__ == "__main__":
    res = annu(float(input('Сумма займа')), float(input('Количество месяцев')), float(input('Процент')))
    print('    ост.    нач. вып.-нач. вып')
    for i in range(len(res)):
        print(f"{i + 1}. ({str(res[i][0])}, {str(res[i][1])}, {str(res[i][2])}, {str(res[i][3])})")
